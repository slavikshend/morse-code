﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;

namespace MorseCode;
class Program
{
    static void Main()
    {
        Action dot = () => {
            int frequency = 600;
            int duration = 300;
            Console.Beep(frequency, duration);
        };
        Action dash = () => {
            int frequency = 600;
            int duration = 900;
            Console.Beep(frequency, duration);
        };
        Dictionary<string, string> morseCodeAlphabet = new Dictionary<string, string> { { "A", ".-" }, { "B", "-..." }, { "C", "-.-." }, { "D", "-.." }, { "E", "." }, { "F", "..-." }, { "G", "--." }, { "H", "...." }, { "I", ".." }, { "J", ".---" },
            { "K", "-.-" }, { "L", ".-.." }, { "M", "--" }, { "N", "-." },
            { "O", "---" }, { "P", ".--." }, { "Q", "--.-" }, { "R", ".-." },
            { "S", "..." }, { "T", "-" }, { "U", "..-" }, { "V", "...-" },
            { "W", ".--" }, { "X", "-..-" }, { "Y", "-.--" }, { "Z", "--.." }, { "0", "-----" }, { "1", ".----" }, { "2", "..---" }, { "3", "...--" },
            { "4", "....-" }, { "5", "....." }, { "6", "-...." }, { "7", "--..." },
            { "8", "---.." }, { "9", "----." }};
        string[] words = Console.ReadLine()!.Trim().ToUpper().Split();
        foreach (var word in words)
        {
            foreach (var letter in word)
            {
                if (morseCodeAlphabet.ContainsKey(letter.ToString()))
                {
                    foreach (var dotdash in morseCodeAlphabet[letter.ToString()])
                    {
                        if (dotdash == '-') dash();
                        if (dotdash == '.') dot();
                        Thread.Sleep(300);
                    }
                }
                Thread.Sleep(900);
            }
            Thread.Sleep(2100);
        }
        Console.WriteLine("Done");
    }
}